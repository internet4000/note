import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import * as serviceWorker from './serviceWorker'

// import all styles
import './styles/code-mirror-main.css' // needs to be first to overwrite
import './styles/html.css'
import './styles/nav.css'
import './styles/storage.css'
import './styles/editor.css'
import './styles/note-card.css'
import './styles/notes.css'
import './styles/contextual-toggle.css'

// start the react app
ReactDOM.render(<App />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister()
