import React from 'react'
// import { Link } from '@reach/router'
import NavLink from './NavLink'

export default (props) => {
	const toHome = props.toHome
	return (
		<nav className={`Nav ${toHome ? 'Nav--toHome' : ''}`}>
			{toHome ? (
				<NavLink
					to="/"
					title="Navigate to the homepage"></NavLink>
			) : (
				<NavLink
					to="/navigation"
					title="Navigate to the menu of this software"></NavLink>
			)}
		</nav>
	)
}
