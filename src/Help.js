import React from 'react'
import { Link } from "@reach/router"
import packageJson from "../package.json"

export default (props) => {

	return (
		<ls-screen >
			<img className="Logo" src={ process.env.PUBLIC_URL + '/note-logo.svg' } alt="Note logo"/>
			<p><i>Welcome to Note!</i></p>
			<p><Link
					 title="Login to store and synchronize your notes on all your devices"
					 to="/user"><strong>Login</strong></Link> to store and synchronize your notes across devices.
			</p>

			<p>The <kbd className="u-yellow">yellow button</kbd> in the corner is the navigation menu; <br/> and here are the keyboard shortcuts:</p>

			<ul>
			<li><kbd>n</kbd> will create a <Link to="/note"><strong>new note</strong></Link> (or <kbd>enter</kbd>, on this page)</li>
			<li><kbd>escape</kbd> will list all your notes</li>
			<li><kbd>h</kbd> brings to this help message</li>
			</ul>

			<p>
			For more features and usage documentation, visit the <Link title="Discover more possibilities" to="/discover">discover</Link> page, with <kbd>d</kbd>.<br/>
			The <a href={packageJson.repository} target="_blank" rel="noopener noreferrer">Git repository</a>, is the place for code, API, and discussion.</p>

			<p><i>You should not use note to store sensitive content.</i></p>
			</ls-screen>
	)
}
