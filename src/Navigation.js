import React from 'react'
import NavLink from './NavLink'
import Nav from './Nav'

export default () => {
	return (
		<nav className='Navigation'>

			<Nav toHome={true}/>

			<ls-screen>
				<NavLink
					title="See all notes"
					to="/notes">All notes <kbd>esc</kbd></NavLink>
				<NavLink
					title="Create a new note"
					to="/note">New note <kbd>n</kbd></NavLink>
				<NavLink
					title="Login to synchronize your notes on all your devices"
					to="/user">User / Login<kbd>u</kbd></NavLink>
				<NavLink
					title="Get help about this software"
					to="/help">Help <kbd>h</kbd></NavLink>
			</ls-screen>
		</nav>
	)
}
