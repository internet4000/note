import React from 'react'
import { Link } from "@reach/router"
import packageJson from "../package.json"

export default () => {
	return (
		<>
			<ls-screen almost>
				<img className="Logo" src={ process.env.PUBLIC_URL + '/note-logo.svg' } alt="Note logo"/>
				<p><i>Ciao, you made it here!</i></p>
				<p>
					If you haven't already <Link title="Login to synchronize your notes on all your devices" to="/user">login</Link> to synchronize your notes on all your devices.</p>
				<p>You can do that with Google Drive, Dropbox, or <a href="https://5apps.com/storage" target="_blank" rel="noopener noreferrer">remote-storage</a>.</p>
				<p>The <kbd className="u-yellow">yellow button</kbd> in the top right corner is the navigation menu.</p>
			</ls-screen>
			<ls-screen almost>
				<ls-container>
					<h2>Keyboard shortcuts</h2>
					<p>The entire application is designed to be keyboard first, as well as mobile first.</p>
					<p>To navigate arround, use the <kbd>TAB</kbd> key of your keyoard, to move from one <strong>link</strong> to an other. <kbd>Shift TAB</kbd> will navigate backwards. If you then hit <kbd>enter</kbd>, it will open the link. Try it on this page, with the following keys of your keyboard.</p>
				<ul>
					<li><kbd>n</kbd> will create a <Link to="/note">new note</Link></li>
			<li><kbd>escape</kbd> will list all your saved notes</li>
			<li><kbd>h</kbd> brings to the help message page</li>
			</ul>
<p>Use <kbd>Ctrl F</kbd> to search every pages, such as on your note page.</p>
			<p>For code, API, and discussion, visit the <a href="https://github.com/hugurp/note" target="_blank" rel="noopener noreferrer">Git repository</a>.</p>
</ls-container>
			</ls-screen>
<ls-screen almost>
<ls-container>
				<h2>Customization</h2>
<p>You can customize the apperence of the entire application, with just CSS. For this, create a note named <Link to="/note/styles.css"><i>styles.css</i></Link>.</p>
<p>If you write CSS in it, it will be used by the application as a normal HTML <code>style</code> element. For example, try making the background color of the HTML body element, blue; and refresh your browser!</p>
</ls-container>
			</ls-screen>
<ls-screen almost>
<ls-container>
				<h2>Create a new note from the url (URL API)</h2>
<p>If you navigate to the url <code>/note/noteSlug</code> you will create a note with the slug and id <code>noteSlug</code></p>
<p>You can as well go to <code>/note?content=noteContent</code> to create a note with the content set as <code>noteContent</code>.</p>
</ls-container>
			</ls-screen>
			<ls-screen>
<ls-container>
<h2>Explore the rest</h2>
<p>For more details on the application code, feature request, API, and discussion, visit the <a href={packageJson.repository} target="_blank" rel="noopener noreferrer">Git repository</a>.</p>
<p>☯</p>
</ls-container>
			</ls-screen>
		</>
	)
}
