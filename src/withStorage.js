import React, { Component } from 'react'
import RemoteStorage from 'remotestoragejs'
import Widget from 'remotestorage-widget'
import NotesModule from './storage-modules/notes';

const withStorage = ExtendedComponent =>
			class Storage extends Component {
				constructor(props) {
					super(props)

					// initialize remote-storage
					this.storage = new RemoteStorage({
						// logging: true
					})

					// claim access to the directory for read+write
					this.storage.access.claim('note4000', 'rw')

					// configure caching and with it automatic sync) for the given path
					// all of the items therein will be automatically synced from and to the server
					// note: without this it seems to delete remote notes after logout/login
					this.storage.caching.enable('/note4000/')

					this.storage.setApiKeys({
						dropbox: 'ibl2lga90i1ubgp',
						googledrive: '77133566319-d76dbmdt9jle773ekplr2k1lrehibt1l.apps.googleusercontent.com'
					});


					// Events
					this.storage.on('connected', () => {
						const userAddress = this.storage.remote.userAddress
						console.debug(`${userAddress} connected their remote storage.`)
					})

					this.storage.on('network-offline', () => {
						console.debug(`We're offline now.`)

					})
					this.storage.on('disconnected', () => {
						// window.location.reload(true)
						console.debug(`We're disconected.`)
					})

					this.storage.on('network-online', () => {
						console.debug('network online')
					})

					// reference module
					this.storage.addModule(NotesModule)

					// create widget
					this.widget = new Widget(this.storage, {
						leaveOpen: true,
						skipInitial: true
					})
				}

				render() {
					return <ExtendedComponent {...this.props} storage={this.storage} widget={this.widget}/>
				}
			}

export default withStorage
