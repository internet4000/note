import React, {Component} from 'react'

class UserStyles extends Component {
	async componentDidMount() {
		const id = 'styles.css'
		let note = await this.props.storage.note4000.get(id)
		if (note) {
			this.setState({
				customStyles: note.data
			})
			console.log('Custom-styles: true')
		} else {
			console.log('Custom-styles: false')
		}
	}
	render () {
		return <style>{this.state ? this.state.customStyles : ''}</style>
	}
}

export default UserStyles
