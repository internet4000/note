import React, { Component } from 'react'
import HotKeysGlobal from './HotKeysGlobal'
import Navigation from './Navigation'
import Nav from './Nav'
import Help from './Help'
import Notes from './Notes'
import Note from './Note'
import NewNote from './NewNote'
import UserStyles from './UserStyles'
import User from './User'
import Discover from './Discover'
import { Router, Redirect } from '@reach/router'
import withStorage from './withStorage'

class App extends Component {
	render() {
		const {storage, widget} = this.props

		return (
			<>
				<HotKeysGlobal>

					<Nav/>

					<UserStyles storage={storage}/>

					<Router className="Router">
						<Redirect from="/" to="/notes"/>

						<Navigation path='/navigation'/>
						<Help path="/help"/>
						<Discover path="/discover"/>
						<User path="/user" storage={storage} widget={widget}/>

						<Notes path="/notes" storage={storage}/>
						<NewNote path="/note" storage={storage}/>
						<Note path="/note/:id" storage={storage}/>
					</Router>

				</HotKeysGlobal>
			</>
		)
	}
}

export default withStorage(App)
