import React, {Component} from 'react'

class User extends Component {
	constructor(props) {
		super(props)
		// create a ref to store the widget DOM element
		this.widgetRef = React.createRef()
	}
	componentDidMount() {
		// Enable widget
		if(this.props.widget) {
			this.props.widget.attach('RemoteStorageWidget')
		}
	}

	render() {
		return (
			<ls-screen>
				<h2>User settings</h2>
				<p>
					Login to synchronize your notes accross devices<br/>
					You can use: Google / Dropbox / remote-storage<br/>
					You should use: <a href="https://5apps.com/storage" target="_blank" rel="noopener noreferrer">remote-storage</a>
				</p>

				<article ref={this.widgetRef} id="RemoteStorageWidget"></article>
			</ls-screen>
		)
	}
}

export default User
