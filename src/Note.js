import React, {Component} from 'react'
import { debounce } from 'lodash'
import { Link } from '@reach/router'
/* import CodeMirrorEditor from './CodeMirrorEditor' */

class Note extends Component {

	// the note id is in the props.id from the URL /:id param
	// we also use state.content for note.content
	constructor(props) {
		super(props)
		this.state = {
			content: ''
		}
	}

	async componentDidMount() {
		let {id} = this.props
		let note = await this.props.storage.note4000.get(id)
		let {content} = this.props.location.state
		if (note) {
			this.setState({
				content: note.data || ''
			})
		}
		if (content) {
			this.setState({
				content: content
			})
		}
	}
	componentWillUnmount() {
		// save one last time before living the note, in case of change
		const content = this.state.content
		if (content) {
			this.saveContent(this.state.content)
		}
  }

	storeNote = (note) => this.props.storage.note4000.storeNote(note)

	debouncedSave = debounce(this.storeNote, 300)

	getCurrentNote = () => {
		return {
			id: this.props.id,
			content: this.state.content
		}
	}

	handleChange = event => {
		event.preventDefault()
		const content = event.target.value
		// set the state to reflect on the UI
		// and save content to reflect the data on storage server
		this.setState({ content })
		return this.saveContent(content)
	}
	saveContent = content =>{
		return this.debouncedSave({
			id: this.props.id,
			content
		})
	}

	handleEditorUnmount = () => this.storeNote(this.getCurrentNote())

	render() {
		const {content} = this.state;
		const {id} = this.props;
		return (
			<article className="Note">
				<nav className="Note-header Menu">
					<Link to="/notes" className="Note-back Menu-item">&larr;</Link>
					<span className="Note-id Menu-item">
						<i>{id}</i>
					</span>
				</nav>

				<textarea
					className="Editor"
					value={content}
					onChange={this.handleChange}>
				</textarea>
			</article>
		)
	}
}

export default Note
