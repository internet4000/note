import React from 'react'
import { navigate } from '@reach/router'

class NewNote extends React.Component {
	componentDidMount() {
		let state = {
			content: this.getContent()
		}
		let id =  this.props.storage.note4000.generateNoteId()

		// TODO: this is the only way to trigger the constructor
		// of the destination component <Note> on /note/:id — why?
		window.setTimeout(() => {
			navigate(`/note/${id}`, {
				replace: true,
				state
			})
		}, 1)
	}
	getContent() {
		var url = new URL(this.props.location.href);
		var content = url.searchParams.get('content');
		if(!content) return ''
		return content
	}
	render() {
		return <></>
	}
}

export default NewNote
