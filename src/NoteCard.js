import React from 'react'
import { Link } from '@reach/router'
import ContextualToggle from './ContextualToggle'
import Option from './Option'

const NoteCard = (props) => {
	let { id, content } = props.note

	return (
		<article className='NoteCard'>
			<Link
				to={`/note/${id}`}
				className="NoteCard-body">
				<span className="NoteCard-content">
					{ content && content.length ? (
						`${content.slice(0, 300)}`
					) : null }
				</span>
				<i className="NoteCard-id">{id}</i>
				<i className="NoteCard-length">.{`${content&& content.length ? content.length : 0}`}</i>
		  </Link>
			<div className="NoteCard-aside">
				<ContextualToggle>
					<Option
						action={ () => props.handleDelete(id) }>Delete</Option>
				</ContextualToggle>
			</div>
		</article>
	)
}

export default NoteCard
