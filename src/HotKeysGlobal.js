import React from 'react';
import { HotKeys } from 'react-hotkeys';
import { navigate } from "@reach/router"

const onKeyUp = (key) => ({ sequence: key, action: 'keyup' })

export default function(props) {
	const keyMap = {
		'gotoNotes': onKeyUp('esc'),
		'gotoNewNote': onKeyUp('n'),
		'gotoHelp': onKeyUp('h'),
		'gotoNavigation': onKeyUp('m'),
		'gotoUser': onKeyUp('u'),
		'gotoDiscover': onKeyUp('d')
	}

	const keyHandlers = {
		'gotoNavigation': () => {
			navigate('/navigation')
		},
		'gotoNotes': () => {
			navigate('/notes')
		},
		'gotoNewNote': () => {
			navigate('/note')
		},
		'gotoHelp': () => {
			navigate('/help')
		},
		'gotoUser': () => {
			navigate('/user')
		},
		'gotoDiscover': () => {
			navigate('/discover')
		}
	}

	return (
		<HotKeys
			focused
			attach={ document }
			handlers={ keyHandlers }
			keyMap={ keyMap }>
			{ props.children }
		</HotKeys>
	)
}
