import React, { Component } from 'react'
import CodeMirror from 'codemirror'
import 'codemirror/addon/display/placeholder'
import { navigate } from "@reach/router"

class CodeMirrorEditor extends Component {
	// FIXME: why is this not called when the transition to this route
	// is made fron @reach/router/navigate() (in <NewNote/>)
	constructor(props) {
		super(props)
		// create a ref to store the editor DOM element
		this.editorRef = React.createRef()
		this.codeMirror = undefined
		this.full = false;
	}

	componentDidMount() {
		this.codeMirror = CodeMirror(this.editorRef.current, {
			value: '',
			mode: 'javascript',
			lineWrapping: true,
			placeholder: 'Write a note — ESC for all notes',
			autofocus: true
		})

		this.codeMirror.setOption("extraKeys", {
			Esc: function() {
				navigate('/notes')
			}
		});

		this.codeMirror.on('change', this.handleCodeMirrorChange)
	}

	componentDidUpdate(prevProps) {
		const { content } = this.props
		const { content: prevContent } = prevProps

		// that's necessary to overwrite the editor when loading a new note
		if (content !== prevContent) {
			const cursor = this.codeMirror.getCursor()
			this.codeMirror.setValue(content)
			this.codeMirror.setCursor(cursor)
		}
	}

	async componentWillUnmount() {
		let editor = this.codeMirror

		try {
			if (editor.getValue()) {
				await this.props.onUnmount()
			}
			editor.off('change', this.handleCodeMirrorChange)
		} catch(e) {
			console.log('error quitting the component')
		}

	}

	handleCodeMirrorChange = editor => {
		return this.props.onChange(editor.getValue())
	}

	render() {
		return (
			<div className="Editor" ref={this.editorRef}></div>
		)
	}
}

export default CodeMirrorEditor
