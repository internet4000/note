import React, {Component} from 'react'
import NoteCard from './NoteCard.js'
import { Link } from "@reach/router";

class Notes extends Component {
	constructor() {
		super()
		this.state = {
			notes: []
		}
	}

	componentDidMount() {
		this.loadNotes()
	}

	loadNotes = async () => {
		let { storage } = this.props
		let notes = await storage.note4000.getAll()
		this.setState({
			notes
		})
	}

	deleteNote = async (id) => {
		let { storage } = this.props
		await storage.note4000.delete(id)
		this.loadNotes()
	}

	render() {
		let { notes } = this.state
		if (!this.state || !notes ) return <ls-screen><i>Loading notes...</i></ls-screen>

		return (
			<>
				{
					notes.length ? (
						<section className="Notes">
							<aside className="Notes-header">
								<i className="Notes-count">{notes.length} {notes.length > 1 ? 'notes' : 'note'}</i>
								<Link className="Notes-new" tabIndex="-1" to='/note'>New <kbd>n</kbd></Link>
							</aside>
							{
								notes
									.map(note =>	{
										return <NoteCard
															 key={note.id}
															 note={note}
															 handleDelete={this.deleteNote}/>
									}).reverse()
							}
						</section>
					) : (
						<ls-screen>
							<p>
								<i>There are no notes</i>
							</p>
							<p>
								To <Link to='/note'>create a new note</Link>, you can press the <kbd>n</kbd> key of your keyboard.
							</p>
						</ls-screen>
					)
				}
			</>
		)
	}
}

export default Notes
